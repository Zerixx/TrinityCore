/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BattlegroundPackets_h__
#define BattlegroundPackets_h__

#include "Packet.h"
#include "LFGPackets.h"

namespace WorldPackets
{
    namespace Battleground
    {
        class PVPSeason final : public ServerPacket
        {
        public:
            PVPSeason() : ServerPacket(SMSG_PVP_SEASON, 8) { }

            WorldPacket const* Write() override;

            uint32 PreviousSeason = 0;
            uint32 CurrentSeason = 0;
        };
    }

    namespace Battlefield
    {

        struct BfStatusHeader
        {
            WorldPackets::LFG::RideTicket RideTicket;
            uint64 queueId = 0;
            uint8 rangeMin = 0;
            uint8 rangeMax = 0;
            uint8 teamSize = 0;
            uint32 instanceId = 0;

            bool registeredMatch = false;
            bool tournamentRules = false;
        };

        class BattlefieldList final : public ServerPacket
        {
        public:
            BattlefieldList() : ServerPacket(SMSG_BATTLEFIELD_LIST, 8) { }

            WorldPacket const* Write() override { return &_worldPacket; }

            uint32 battlefieldsCount = 0;

            ObjectGuid BattlemasterGuid;
            uint32 BattlemasterListId = 0;
            uint8 MinLevel = 0;
            uint8 MaxLevel = 0;
            std::vector<uint32> Battlefields;
            bool HasHolidayWinToday = false;
            bool HasRandomWinToday = false;
            bool PvpAnywhere = false;
            bool IsRandomBg = false;
        };

        class BfStatusQueued final : public ServerPacket
        {
        public:
            BfStatusQueued() : ServerPacket(SMSG_BATTLEFIELD_STATUS_QUEUED) { }

            WorldPacket const* Write() override { return &_worldPacket; }

            WorldPackets::Battlefield::BfStatusHeader header;

            uint32 averageWaitTime = 0;
            uint32 waitTime = 0; // time in que so far
            bool asGroup = false;
            bool suspendedQueue = false;
            bool elegibleForMatchmaking = false;
        };

        class BfList final : public ClientPacket
        {
        public:
            BfList(WorldPacket&& packet) : ClientPacket(CMSG_BATTLEFIELD_LIST, std::move(packet)) { }

            void Read();

            uint32 listId = 0;
        };
    }
}

#endif // BattlegroundPackets_h__
