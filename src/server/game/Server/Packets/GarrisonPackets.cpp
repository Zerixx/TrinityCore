/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "GarrisonPackets.h"

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::Vector3 const& v3)
{
    data << float(v3.x);
    data << float(v3.y);
    data << float(v3.z);

    return data;
}

ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::Vector3& v3)
{
    data >> float(v3.x);
    data >> float(v3.y);
    data >> float(v3.z);

    return data;
}

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonBuildingInfo const& buildingInfo)
{
    data << uint32(buildingInfo.GarrPlotInstanceID)
        << uint32(buildingInfo.GarrBuildingID)
        << uint32(buildingInfo.TimeBuilt)
        << bool(buildingInfo.Active)
        << WorldPackets::Garrison::Vector3(buildingInfo.BuildingPos)
        << float(buildingInfo.BuildingFacing)
        << uint32(buildingInfo.CurrentGarSpecID);
    
    return data;
}

ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonBuildingInfo& buildingInfo)
{
    data >> uint32(buildingInfo.GarrPlotInstanceID)
        >> uint32(buildingInfo.GarrBuildingID)
        >> uint32(buildingInfo.TimeBuilt)
        >> bool(buildingInfo.Active)
        >> WorldPackets::Garrison::Vector3(buildingInfo.BuildingPos)
        >> float(buildingInfo.BuildingFacing)
        >> uint32(buildingInfo.CurrentGarSpecID);

    return data;
}

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonFollower const& GarrisonFollower)
{
    data << uint64(GarrisonFollower.DbID)
        << uint32(GarrisonFollower.GarrFollowerID)
        << uint32(GarrisonFollower.CreatureID)
        << uint32(GarrisonFollower.GarrGivenNameID)
        << uint32(GarrisonFollower.GarrFamilyNameID)
        << uint32(GarrisonFollower.Gender)
        << uint32(GarrisonFollower.Spec)
        << uint32(GarrisonFollower.Race)
        << uint32(GarrisonFollower.Quality)
        << uint32(GarrisonFollower.FollowerLevel)
        << uint32(GarrisonFollower.ItemLevelWeapon)
        << uint32(GarrisonFollower.ItemLevelArmor)
        << uint32(GarrisonFollower.Xp)
        << uint32(GarrisonFollower.CurrentBuildingID)
        << uint32(GarrisonFollower.CurrentMissionID)
        << std::vector<uint32>(GarrisonFollower.AbilityID);

    return data;
}

ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonFollower& GarrisonFollower)
{
    data >> uint64(GarrisonFollower.DbID)
        >> uint32(GarrisonFollower.GarrFollowerID)
        >> uint32(GarrisonFollower.CreatureID)
        >> uint32(GarrisonFollower.GarrGivenNameID)
        >> uint32(GarrisonFollower.GarrFamilyNameID)
        >> uint32(GarrisonFollower.Gender)
        >> uint32(GarrisonFollower.Spec)
        >> uint32(GarrisonFollower.Race)
        >> uint32(GarrisonFollower.Quality)
        >> uint32(GarrisonFollower.FollowerLevel)
        >> uint32(GarrisonFollower.ItemLevelWeapon)
        >> uint32(GarrisonFollower.ItemLevelArmor)
        >> uint32(GarrisonFollower.Xp)
        >> uint32(GarrisonFollower.CurrentBuildingID)
        >> uint32(GarrisonFollower.CurrentMissionID)
        >> std::vector<uint32>(GarrisonFollower.AbilityID);

    return data;
}

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonMission const& GarrisonMission)
{
    data << uint64(GarrisonMission.DbId)
        << uint32(GarrisonMission.MissionRecID)
        << uint32(GarrisonMission.OfferTime)
        << uint32(GarrisonMission.OfferDuration)
        << uint32(GarrisonMission.StartTime)
        << uint32(GarrisonMission.TravelDuration)
        << uint32(GarrisonMission.MissionDuration)
        << uint32(GarrisonMission.MissionState);

    return data;
}

ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonMission& GarrisonMission)
{
    data >> uint64(GarrisonMission.DbId)
        >> uint32(GarrisonMission.MissionRecID)
        >> uint32(GarrisonMission.OfferTime)
        >> uint32(GarrisonMission.OfferDuration)
        >> uint32(GarrisonMission.StartTime)
        >> uint32(GarrisonMission.TravelDuration)
        >> uint32(GarrisonMission.MissionDuration)
        >> uint32(GarrisonMission.MissionState);

    return data;
}

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonPlotInfo const& GarrisonPlotInfo)
{
    data << uint32(GarrisonPlotInfo.GarrPlotInstanceID)
        << WorldPackets::Garrison::Vector3(GarrisonPlotInfo.PlotPos)
        << uint32(GarrisonPlotInfo.PlotType);

    return data;
}

ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonPlotInfo& GarrisonPlotInfo)
{
    data >> uint32(GarrisonPlotInfo.GarrPlotInstanceID)
        >> WorldPackets::Garrison::Vector3(GarrisonPlotInfo.PlotPos)
        >> uint32(GarrisonPlotInfo.PlotType);

    return data;
}

WorldPacket const* WorldPackets::Garrison::GarrisonOpenArchitect::Write()
{
    _worldPacket << npcGuid;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonRemoteInfo::Write()
{
    _worldPacket << infoCount;
    _worldPacket << buildings.GarrisonBuildingId; 

    for (uint32 i = 0; i < infoCount; i++)
        _worldPacket << sites.GarrisonSiteLevelId;

    for (uint32 i = 0; i < buildings.GarrisonBuildingId; i++)
        _worldPacket << buildings.GarrisonPlotInstanceId;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonGetInfoResult::Write()
{
    _worldPacket << GarrSiteID
     << GarrSiteLevelID
     << FactionIndex;

    _worldPacket << GarrisonBuildingInfoCount
        << GarrisonPlotInfoCount
        << GarrisonFollowerCount
        << GarrisonMissionCount
        << ArchivedMissionsCount;

    _worldPacket << GarrUnk1;

    for (uint32 i = 0; i < GarrisonBuildingInfoCount; i++)
        _worldPacket << buildingInfo;

    for (uint32 i = 0; i < GarrisonPlotInfoCount; i++)
        _worldPacket << plot;

    for (uint32 i = 0; i < GarrisonFollowerCount; i++)
        _worldPacket << follower;

    for (uint32 i = 0; i < GarrisonMissionCount; i++)
        _worldPacket << mission;

    for (uint32 i = 0; i < ArchivedMissionsCount; i++)
        _worldPacket << archivedMissions;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonLearnBlueprintResult::Write()
{
    _worldPacket << BuildingID;
    _worldPacket << Result;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonAddFollowerResult::Write()
{
    _worldPacket << follower;
    _worldPacket << result;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonRemoveFollowerResult::Write()
{
    _worldPacket << DbId;
    _worldPacket << result;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonUpgradeableResult::Write()
{
    _worldPacket << result;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonCompleteMissionResult::Write()
{
    _worldPacket << missionRecId;
    _worldPacket << mission;
    _worldPacket << result;

    return &_worldPacket;
}

void WorldPackets::Garrison::GarrisonCompleteMission::Read()
{
    _worldPacket << npcGUID;
    _worldPacket << missionRecId;
}

WorldPacket const* WorldPackets::Garrison::GarrisonFollowerChangedXp::Write()
{
    _worldPacket << result;
    _worldPacket << follower; // Follower xp before
    _worldPacket << follower; // Follower xp after

    return &_worldPacket;
}

void WorldPackets::Garrison::GarrisonMissionBonusRoll::Read()
{
    _worldPacket << npcGuid;
    _worldPacket << missionRecId;
}

WorldPacket const* WorldPackets::Garrison::GarrisonCompleteMissionResult::Write()
{
    _worldPacket << mission;
    _worldPacket << missionRecId;
    _worldPacket << result;

    return &_worldPacket;
}

void WorldPackets::Garrison::GarrisonStartMission::Read()
{
    _worldPacket << npcGuid;
    _worldPacket << missionRecId;
    
    for (uint32 i = 0; i < infoCount; i++)
        _worldPacket << followerDbid;
}

WorldPacket const* WorldPackets::Garrison::GarrisonStartMissionResult::Write()
{
    _worldPacket << result;
    _worldPacket << mission;

    for (uint32 i = 0; i < followerCount; i++)
        _worldPacket << followerDbid;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::Garrison::GarrisonLandingpageShipments::Write()
{
    for (uint32 i = 0; i < count; i++)
    {
        _worldPacket << missionRecId
            << followerDbid
            << unk1
            << unk2;
    }

    return &_worldPacket;
}