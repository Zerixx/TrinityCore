/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GarrisonPackets_h__
#define GarrisonPackets_h__

#include "WorldPacket.h"
#include "Packet.h"

namespace WorldPackets
{
    namespace Garrison
    {
        struct Vector3
        {
            float x;
            float y;
            float z;
        };
       
        struct GarrisonRemoteBuildingInfo
        {
            uint32 GarrisonPlotInstanceId;
            uint32 GarrisonBuildingId;
        };

        struct GarrisonRemoteSiteInfo
        {
            uint32 GarrisonSiteLevelId;
            std::vector<GarrisonRemoteBuildingInfo> Buildings;
        };

        struct GarrisonMission
        {
            uint64 DbId;
            uint32 MissionRecID;
            uint32 OfferTime;
            uint32 OfferDuration;
            uint32 StartTime;
            uint32 TravelDuration;
            uint32 MissionDuration;
            uint32 MissionState;
        };

        struct GarrisonBuildingInfo
        {
            uint32 GarrPlotInstanceID;
            uint32 GarrBuildingID;
            uint32 TimeBuilt;
            bool Active = false;
            Vector3 BuildingPos;
            float BuildingFacing;
            uint32 CurrentGarSpecID;
        };

        struct GarrisonFollower
        {
            uint64 DbID;
            uint32 GarrFollowerID;
            uint32 CreatureID;
            uint32 GarrGivenNameID;
            uint32 GarrFamilyNameID;
            uint32 Gender;
            uint32 Spec;
            uint32 Race;
            uint32 Quality;
            uint32 FollowerLevel;
            uint32 ItemLevelWeapon;
            uint32 ItemLevelArmor;
            uint32 Xp;
            uint32 CurrentBuildingID;
            uint32 CurrentMissionID;
            std::vector <uint32> AbilityID;
        };

        struct GarrisonPlotInfo
        {
            uint32 GarrPlotInstanceID;
            Vector3 PlotPos;
            uint32 PlotType;
        };
        
        // SMSG_GARRISON_OPEN_ARCHITECT
        class GarrisonOpenArchitect final : public ServerPacket
        {
        public:
            GarrisonOpenArchitect() : ServerPacket(SMSG_GARRISON_OPEN_ARCHITECT) { } // unk len

            WorldPacket const* Write() override;

            ObjectGuid npcGuid;
            Creature creature;
        };

        // SMSG_GARRISON_REMOTE_INTO
        class GarrisonRemoteInfo final : public ServerPacket
        {
        public:
            GarrisonRemoteInfo() : ServerPacket(SMSG_GARRISON_REMOTE_INFO) { }

            WorldPacket const* Write() override;

            GarrisonRemoteSiteInfo sites;
            GarrisonRemoteBuildingInfo buildings;

            uint32 infoCount = 0;
            uint32 unk16 = 0;
            uint32 unk5 = 0;
        };

        // SMSG_GET_GARRISON_INFO_RESULT
        class GarrisonGetInfoResult final : public ServerPacket
        {
        public:
            GarrisonGetInfoResult() : ServerPacket(SMSG_GET_GARRISON_INFO_RESULT) { }

            WorldPacket const* Write() override;

            std::vector<uint32> archivedMissions;
            uint32 GarrSiteID = 0;
            uint32 FactionIndex = 0;
            uint32 GarrSiteLevelID = 0;
            uint32 GarrUnk1 = 0;

            uint32 GarrisonBuildingInfoCount = 0;
            uint32 GarrisonPlotInfoCount = 0;
            uint32 GarrisonFollowerCount = 0;
            uint32 GarrisonMissionCount = 0;
            uint32 ArchivedMissionsCount = 0;

            std::vector<GarrisonMission> mission;
            std::vector<GarrisonBuildingInfo> buildingInfo;
            std::vector<GarrisonFollower> follower;
            std::vector<GarrisonPlotInfo> plot;
        };

        // SMSG_GARRISON_LEARN_BLUEPRINT_RESULT
        class GarrisonLearnBlueprintResult final : public ServerPacket
        {
        public:
            GarrisonLearnBlueprintResult() : ServerPacket(SMSG_GARRISON_LEARN_BLUEPRINT_RESULT) { }
            
            WorldPacket const* Write() override;

            uint32 BuildingID = 0;
            uint32 Result = 0;
        };

        // SMSG_GARRISON_ADD_FOLLOWER_RESULT
        class GarrisonAddFollowerResult final : public ServerPacket
        {
        public:
            GarrisonAddFollowerResult() : ServerPacket(SMSG_GARRISON_ADD_FOLLOWER_RESULT) { }

            WorldPacket const* Write() override;

            GarrisonFollower follower;
            uint32 result = 0;
        };

        // SMSG_GARRISON_REMOVE_FOLLOWER_RESULT
        class GarrisonRemoveFollowerResult final : public ServerPacket
        {
        public:
            GarrisonRemoveFollowerResult() : ServerPacket(SMSG_GARRISON_REMOVE_FOLLOWER_RESULT) { }

            WorldPacket const* Write() override;

            uint64 DbId = 0;
            uint32 result = 0;
        };

        class GarrisonGetInfo final : public ClientPacket
        {
        public:
            GarrisonGetInfo(WorldPacket&& packet) : ClientPacket(CMSG_GET_GARRISON_INFO, std::move(packet)) { }

            void Read() override { }
        };

        // CMSG_GARRISON_CHECK_UPGRADEABLE 
        class GarrisonCheckUpgradeable final : public ClientPacket
        {
        public:
            GarrisonCheckUpgradeable(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_REQUEST_UPGRADEABLE, std::move(packet)) { }

            void Read() override { }
        };

        // SMSG_GARRISON_IS_UPGRADEABLE_RESULT
        class GarrisonUpgradeableResult final : public ServerPacket
        {
        public:
            GarrisonUpgradeableResult() : ServerPacket(SMSG_GARRISON_IS_UPGRADEABLE_RESULT, 4) { }

            WorldPacket const* Write() override;

            uint32 result = 0;
        };

        // CMSG_GARRISON_COMPLETE_MISSION
        class GarrisonCompleteMission final : public ClientPacket
        {
        public:
            GarrisonCompleteMission(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_COMPLETE_MISSION, std::move(packet)) { }

            void Read() override { }

            ObjectGuid npcGUID;
            uint32 missionRecId;
        };

        // SMSG_GARRISON_COMPLETE_MISSION_RESULT
        class GarrisonCompleteMissionResult final : public ServerPacket
        {
        public:
            GarrisonCompleteMissionResult() : ServerPacket(SMSG_GARRISON_COMPLETE_MISSION_RESULT) { }

            WorldPacket const* Write() override;

            GarrisonMission mission;
            uint32 missionRecId = 0;
            uint32 result = 0;
        };

        // SMSG_GARRISON_FOLLOWER_CHANGED_XP
        class GarrisonFollowerChangedXp final : public ServerPacket
        {
        public:
            GarrisonFollowerChangedXp() : ServerPacket(SMSG_GARRISON_FOLLOWER_CHANGED_XP) { }

            WorldPacket const* Write() override;

            GarrisonFollower follower;
            uint32 result = 0;
        };

        // CMSG_GARRISON_MISSION_BONUS_ROLL
        class GarrisonMissionBonusRoll final : public ClientPacket
        {
        public:
            GarrisonMissionBonusRoll(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_MISSION_BONUS_ROLL, std::move(packet)) { }

            void Read() override { }

            ObjectGuid npcGuid;
            uint32 missionRecId = 0;
        };

        // SMSG_GARRISON_MISSION_BONUS_ROLL_RESULT
        class GarrisonMissionBonusRollResult final : public ServerPacket
        {
        public:
            GarrisonMissionBonusRollResult() : ServerPacket(SMSG_GARRISON_MISSION_BONUS_ROLL_RESULT) { }

            WorldPacket const* Write() override;

            GarrisonMission mission;

            uint32 result = 0;
            uint32 missionRecId = 0;
        };

        // CMSG_GARRISON_START_MISSION
        class GarrisonStartMission final : public ClientPacket
        {
        public:
            GarrisonStartMission(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_START_MISSION, std::move(packet)) { }

            void Read() override { }

            ObjectGuid npcGuid;
            uint32 infoCount = 0;
            std::vector<uint64> followerDbid;
            uint32 missionRecId = 0;
        };

        // SMSG_GARRISON_START_MISSION_RESULT
        class GarrisonStartMissionResult final : public ServerPacket
        {
        public:
            GarrisonStartMissionResult() : ServerPacket(SMSG_GARRISON_START_MISSION_RESULT) { }

            WorldPacket const* Write() override;

            ObjectGuid npcGuid;
            GarrisonMission mission;
            uint32 followerCount = 0;
            std::vector<uint64> followerDbid;
            uint32 result = 0;
        };

        // CMSG_GARRISON_REQUEST_LANDING_PAGE_SHIPMENT_INFO
        class GarrisonRequestLandingPageShipmentInfo final : public ClientPacket
        {
        public:
            GarrisonRequestLandingPageShipmentInfo(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_REQUEST_LANDING_PAGE_SHIPMENT_INFO, std::move(packet)) { }

            void Read() override { }
        };

        // SMSG_GARRISON_LANDINGPAGE_SHIPMENTS 
        class GarrisonLandingpageShipments final : public ServerPacket
        {
        public:
            GarrisonLandingpageShipments() : ServerPacket(SMSG_GARRISON_LANDINGPAGE_SHIPMENTS) { }

            WorldPacket const* Write() override;

            uint32 count = 0;
            uint32 missionRecId = 0;
            uint32 unk1 = 0;
            uint32 unk2 = 0;
            uint64 followerDbid = 0;
        };

        // CMSG_GARRISON_REQUEST_BLUEPRINT_AND_SPECIALIZATION_DATA
        class GarrisonRequestBlueprintAndSpecData final : public ClientPacket
        {
        public:
            GarrisonRequestBlueprintAndSpecData(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_REQUEST_BLUEPRINT_AND_SPECIALIZATION_DATA, std::move(packet)) { }

            void Read() override { }
        };

        // CMSG_GARRISON_GET_BUILDING_LANDMARKS
        class GarrisonGetBuildingLandmarks final : public ClientPacket
        {
        public: 
            GarrisonGetBuildingLandmarks(WorldPacket&& packet) : ClientPacket(CMSG_GARRISON_GET_BUILDING_LANDMARKS, std::move(packet)) { }

            void Read() override { }
<<<<<<< Updated upstream
            // Weird opcode. Shares the same hex value as CMSG_GET_GARRISON_INFO, and yields individual outcome with SMSG_GARRISON_BUILDING_LANDMARKS... fascinating.
        };

        // SMSG_GARRISON_BUILDING_LANDMARKS 
        class GarrisonBuildingLandmarks final : public ServerPacket
        {
        public:
            GarrisonBuildingLandmarks() : ServerPacket(SMSG_GARRISON_BUILDING_LANDMARKS) { }

            WorldPacket const* Write() override;


=======
>>>>>>> Stashed changes
        };
    }
}

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonMission const& GarrisonMission);
ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonMission& GarrisonMission);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonFollower const& GarrisonFollower);
ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonFollower& GarrisonFollower);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonBuildingInfo const& GarrisonBuildingInfo);
ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonBuildingInfo& GarrisonBuildingInfo);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::GarrisonPlotInfo const& GarrisonPlotInfo);
ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::GarrisonPlotInfo& GarrisonPlotInfo);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::Garrison::Vector3 const& Vector3);
ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::Garrison::Vector3& Vector3);

#endif