/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRINITY_GARRISON_H
#define TRINITY_GARRISON_H

#include "Player.h"
#include "Chat.h"
#include "Common.h"
#include "DB2Structure.h"
#include "DB2Stores.h"

enum GarrSpells
{
    SPELL_FAILED_GARRISON_NO_MISSIONS_AVAILABLE             = 0,
    SPELL_FAILED_GARRISON_MISSION_COMPLETE                  = 1,
    SPELL_FAILED_GARRISON_MISSION_NOT_IN_PROGRESS           = 2,
    SPELL_FAILED_GARRISON_FOLLOWER_NO_OVERRIABLE_ABILITY    = 3,
    SPELL_FAILED_GARRISON_FOLLOWER_HAS_ABILITY              = 4,
    SPELL_FAILED_GARRISON_FOLLOWER_NOT_MAX_LEVEL            = 5,
    SPELL_FAILED_GARRISON_FOLLOWER_MAX_QUALITY              = 6,
    SPELL_FAILED_GARRISON_FOLLOWER_MAX_ITEM_LEVEL           = 7,
    SPELL_FAILED_GARRISON_FOLLOWER_MAX_LEVEL                = 8,
    SPELL_FAILED_GARRISON_FOLLOWER_IN_BUILDING              = 9,
    SPELL_FAILED_GARRISON_FOLLOWER_ON_MISSION               = 10,
    SPELL_FAILED_GARRISON_NOT_UPGRAABLE                     = 11,
    SPELL_FAILED_GARRISON_MAX_LEVEL                         = 12,
    SPELL_FAILED_GARRISON_OWNED                             = 13,
    SPELL_FAILED_GARRISON_NOT_OWNED                         = 14
};

enum GarrisonShipmentStatus
{
    GARRISON_SHIPMENT_READY       = 0,
    GARRISON_SHIPMENT_EMPTY       = 1,
    GARRISON_SHIPMENT_IN_PROGRESS = 2
};

enum GarrisonTrophy
{
    GARRISON_TROPHY_LOCKED_SUBTEXT       = 0,
    GARRISON_TROPHY_NOT_SELECTED_TOOLTIP = 1
};

enum GarrAbilityCategory
{
    GARR_ABILITY_CATEGORY_NONE              = 0,
    GARR_ABILITY_CATEGORY_SLAYER            = 1,
    GARR_ABILITY_CATEGORY_RACIAL_PREFERENCE = 2,
    GARR_ABILITY_CATEGORY_PROFESSION        = 3,
    GARR_ABILITY_CATEGORY_OTHER             = 4,
    GARR_ABILITY_CATEGORY_INCREASED_REWARDS = 5,
    GARR_ABILITY_CATEGORY_MISSION_DURATION  = 6,
    GARR_ABILITY_CATEGORY_ENV_PREFERENCE    = 7
};

enum GarrBuildingCategory
{
    GARR_BUILDING_SMALL                     = 0,
    GARR_BUILDING_MEDIUM                    = 1,
    GARR_BUILDING_LARGE                     = 2,
    GARR_BUILDING_SPECIAL                   = 3         // -- Menagerie/Mine/Fishing Hut
};

enum GarrPlotCategory
{
    GARR_PLOT_SMALL                         = 0,
    GARR_PLOT_MEDIUM                        = 1,
    GARR_PLOT_LARGE                         = 2,
    GARR_PLOT_MINE                          = 3,
    GARR_PLOT_FARM                          = 4,
    GARR_PLOT_FISHING_HUT                   = 5,
    GARR_PLOT_PET_MENAGERIE                 = 6
};

enum GarrMonuments
{
    GARRISON_MONUMENT_REPLACED                  = 0,
    GARRISON_MONUMENT_SELECTED_TROPHY_ID_LOAD   = 1,
    GARRISON_MONUMENT_LIST_LOAD                 = 2,
    GARRISON_MONUMENT_CLOSE_UI                  = 3,
    GARRISON_MONUMENT_SHOW_UI                   = 4
};

enum MissionTypes
{
    GARR_MISSION_COMBAT                     = 0,
    GARR_MISSION_GENERIC                    = 1,
    GARR_MISSION_SALVAGE                    = 2,
    GARR_MISSION_LOGISTICS                  = 3,
    GARR_MISSION_WILDLIFE                   = 4,
    GARR_MISSION_TRADING                    = 5,
    GARR_MISSION_CONSTRUCTION               = 6,
    GARR_MISSION_PROVISION                  = 7,
    GARR_MISSION_RECRUITMENT                = 8,
    GARR_MISSION_TRAINING                   = 9,
    GARR_MISSION_PATROL                     = 10,
    GARR_MISSION_RESEARCH                   = 11,
    GARR_MISSION_DEFENSE                    = 12,
    GARR_MISSION_EXPLORATION                = 13,
    GARR_MISSION_SIEGE                      = 14,
    GARR_MISSION_ALCHEMY                    = 15,
    GARR_MISSION_BLACKSMITHING              = 16,
    GARR_MISSION_ENCHANTING                 = 17,
    GARR_MISSION_ENGINEERING                = 18,
    GARR_MISSION_INSRIPTION                 = 19,
    GARR_MISSION_JEWELCRAFTING              = 20,
    GARR_MISSION_LEATHERWORKING             = 21,
    GARR_MISSION_TAILORING                  = 22,
    GARR_MISSION_TREASURE                   = 23,
    GARR_MISSION_PET_BATTLE                 = 24
};

enum MissionReward
{
    GARRISON_REWARD_MONEY                   = 0,
    GARRISON_REWARD_CURRENCY                = 1,
    GARRISON_REWARD_XP                      = 2
};

enum GarrFollowerState
{
    GARRISON_FOLLOWER_EXHAUSTED             = 0,
    GARRISON_FOLLOWER_IN_PARTY              = 1,
    GARRISON_FOLLOWER_WORKING               = 2,
    GARRISON_FOLLOWER_ON_MISSION            = 3,
    GARRISON_FOLLOWER_INACTIVE              = 4,
    GARRISON_FOLLOWER_XP_AED_SHIPMENT       = 5
};

enum GarrisonDuration
{
    GARRISON_DURATION_SECONDS               = 0,
    GARRISON_DURATION_MINUTES               = 1,
    GARRISON_DURATION_HOURS                 = 2,
    GARRISON_DURATION_HOURS_MINUTES         = 3,
    GARRISON_DURATION_DAYS                  = 4,
    GARRISON_DURATION_DAYS_HOURS            = 5
};

enum GarrisonErrors
{
    ERR_GARRISON_BUILDING_EXISTS            = 0,
    ERR_GARRISON_INVALID_PLOT               = 1,
    ERR_GARRISON_INVALID_BUILDINGID         = 2,
    ERR_GARRISON_INVALID_PLOT_BUILDING      = 3,
    ERR_GARRISON_REQUIRES_BLUEPRINT         = 4,
    ERR_GARRISON_NOT_ENOUGH_CURRENCY        = 5,
    ERR_GARRISON_NOT_ENOUGH_GOLD            = 6
};

class Garrison
{
    class Followers
    {
    public:

    private:

    };
};

#endif